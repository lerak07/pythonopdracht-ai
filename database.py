from mysql import connector
import bcrypt
class Database():
    def __init__(self, username, password, url, database):
        self.userid = None
        self.database= connector.connect(
            host=url,
            user=username,
            password=password,
            database=database)
        self.cursor = self.database.cursor()
        self.setup()

    def __exit__(self):
        self.database.close()

    def setup(self):
        self.cursor.execute("CREATE TABLE IF NOT EXISTS users (username VARCHAR(255) PRIMARY KEY, password VARCHAR(255))")
        self.cursor.execute("CREATE TABLE IF NOT EXISTS scores ( level_id int, username  VARCHAR(255) , coins int, finish_time float, FOREIGN KEY(username) REFERENCES users(username), CONSTRAINT PK_SCORES PRIMARY KEY (username, level_id) )")


    def insertNewScore(self, level,  coins, time ):
       
        self.cursor.execute("""INSERT INTO scores (level_id, username, coins, finish_time ) values (%s, %s, %s, %s)""", (level, self.userid, coins, time))
        self.database.commit()
    def updateLevelScore(self, level, coins, time):
        self.cursor.execute(""" UPDATE scores SET coins = %s, finish_time = %s where username = %s and level_id = %s""", (coins,time,self.userid,level))
        self.database.commit()
          

    def getScore(self, level):
        self.cursor.execute("""SELECT * FROM scores WHERE level_id = %s and username = %s ORDER BY finish_time DESC """, (level,self.userid))
        return self.cursor.fetchone()
    def getScoresOrderedbytime(self, level):
        
        self.cursor.execute("""SELECT * FROM scores WHERE level_id = %s ORDER BY finish_time  """, (level,))
        return self.cursor.fetchall()
    




    #login functions
    def registerUser(self, username, password):
        # Password hashing
        password = self.get_hashed_password(password.encode())
        # SQL injection proof ;D
        try: 
            self.cursor.execute("""INSERT INTO users (username, password ) values (%s , %s )""", (username, password))
            self.database.commit()
            self.userid = username
        except Exception as e:
            if e.args[0] == 1062 :
                return 2
            return 3
        return 1


    def getUser(self, username, password):
        self.cursor.execute("""SELECT * FROM users WHERE username = %s""", (username,))
        user = self.cursor.fetchone()
        
        # return true if username and password checks out

        if user != None and self.check_password(password, user[1]) :
            self.userid = user[0]
            return True
        else: return False


    @staticmethod
    def get_hashed_password(password):
        # gen salt + hashed password
        return bcrypt.hashpw(password, bcrypt.gensalt())

    @staticmethod
    def check_password(plain_text_password, hashed_password):
        
        return bcrypt.checkpw(plain_text_password.encode(), hashed_password.encode())

