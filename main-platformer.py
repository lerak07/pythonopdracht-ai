
from database import Database
import pygame
from pygame.locals import *
from pygame import mixer
import pickle
import os
from os import path
from dotenv import load_dotenv
import time
import csv
import getpass

#load in environment vars
load_dotenv()
# initialise database
database = Database (
        username= os.getenv('DB_USERNAME'),
        password= os.getenv('DB_PASSWORD'),
        url= os.getenv('DB_URL'),
        database= os.getenv('DB_DBNAME'))
# this setups the right tables in the database
database.setup()
# login part
logged_in = False
input_choice = 4
username=''
password = ''


class User_login():
    def __init__(self):
        self.username = ''
        self.password = ''
        self.coin_score = 0
        self.time_score = 0,000
        self.show_results = False

    def main_menu_cli(self):
        input_choice = 4
        while input_choice > 3 or input_choice < 1:
            print('- To register press 1')
            print('- To login press 2')
            print('- To exit press 3')
            try:
                input_choice = int(input('accepted input between 1-3: '))
            except ValueError:
                pass
            if input_choice > 3 or input_choice < 1:
                print('\n'* 6)

                print('bad input learn to read!\n')
        
        return input_choice

    def register_user_cli(self):
        input_choice=0
        print('register!')
        self.username = input('Username: ')
        self.password = getpass.getpass('Password: ')
        passcheck = getpass.getpass('Passwordcheck: ')

        if self.password == '' or self.username == '':
            print('Please do not leave username and/or password blank!')
        elif passcheck != self.password:
            print('Password and passcheck need to match!')
        else:
            dbcheck = database.registerUser(self.username, self.password)
            if dbcheck ==1 :
                input_choice = 5
                self.password = ''               
                
            elif dbcheck == 3 :
                print('registering failed')
            elif dbcheck == 2:
                print('username taken try another one!')
            if dbcheck != 1:
                input_choice = 4
        print(self.username)        
        return input_choice
    def login_user_cli (self):
        input_choice = 0
        print('login!')
        self.username = input('Username: ')
        self.password = getpass.getpass('Password: ')
        

        if self.password == '' or self.username == '':
            print('Please do not leave username and/or password blank!')
        else:
            dbcheck = database.getUser(self.username, self.password)
            if dbcheck:
                print('logged_in')
                input_choice = 5
            else:
                print('no login')
                input_choice = 4
        return input_choice
    def addScore(self, level_id):      

       current_score = database.getScore(level_id)
       
       if current_score == None:
        #    print('new score')
           database.insertNewScore(level_id, self.coin_score, self.time_score)
        
       else:
           if current_score[2] < self.coin_score and current_score[3] > self.time_score:
                database.updateLevelScore(level_id, self.coin_score,self.time_score)
                # print('update score and time coins')

           elif current_score[2] < self.coin_score:
                database.updateLevelScore(level_id, self.coin_score,current_score[3])
                # print('update score coins')
               
           elif current_score[3] > self.time_score:
               database.updateLevelScore(level_id, current_score[2], self.time_score)
            #    print('update score times')
    def show_highscores(self):
        current_level =0
        # print(highscores)

        for x in range(max_levels+1):
            highscores = database.getScoresOrderedbytime(current_level)

            print(f'\nHighscores ordered by time level {x}: \n')
            print('Username  Coin_score  Time_score')
            # print(highscores[x])
            counter = 1
            for y in highscores:
                print(f'{counter}.{y[1]}        {y[2]}       {y[3]} ')
                counter +=1
            current_level+=1
    def export_highscores(self):
        current_level =0
        csv_header = ['level','username', 'coin_score', 'time_score']
        # print(highscores)

        for x in range(max_levels+1):
            highscores = database.getScoresOrderedbytime(current_level)
            file = open(f'highscores_level{x}.csv', 'w', encoding='UTF8')
            with file:
                writer = csv.writer(file)

                writer.writerow(csv_header)

                writer.writerows(highscores)            
            
          
            current_level+=1

            



# code to execute before launch of pygame

user = User_login()
while not logged_in:
    input_choice = user.main_menu_cli()
    while input_choice == 1:
        input_choice = user.register_user_cli()
        if input_choice == 5:           
            logged_in = True
    while input_choice == 2:
        input_choice = user.login_user_cli()
        if input_choice == 5:           
            logged_in = True      
    if input_choice == 3:
        exit()
        
# start of pygame execution    
pygame.mixer.pre_init(44100, -16, 2, 512)
mixer.init()
pygame.init()

clock = pygame.time.Clock()
fps = 60
screen_width = 1000
screen_height = 1000
screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption('Platformer')

# define fonts
font_score = pygame.font.SysFont('Bauhaus 93', 30)
font = pygame.font.SysFont('Bauhaus 93', 70)
FONT = pygame.font.SysFont('Verdana', 50)

#define game variables
tile_size = 50
game_over = 0
main_menu = True
level = 0
max_levels = 7
score = 0

#define colours
white = (255, 255, 255)
blue = (0, 0, 255 )
# load images in

sun_image = pygame.image.load('img/sun.png')
sky_image = pygame.image.load('img/sky.png')
restart_image = pygame.image.load('img/restart_btn.png')
start_image = pygame.image.load('img/start_btn.png')
exit_image = pygame.image.load('img/exit_btn.png')
# http://pixelartmaker.com/art/aabd39f52f489fa
highscores_image = pygame.image.load('img/highscores.png')

# load sounds

pygame.mixer.music.load('img/music.wav')
pygame.mixer.music.set_volume(0.05)
pygame.mixer.music.play(-1,0.0,5000)

coin_fx = pygame.mixer.Sound('img/coin.wav')
coin_fx.set_volume(0.05)

jump_fx = pygame.mixer.Sound('img/jump.wav')
jump_fx.set_volume(0.05)

game_over_fx = pygame.mixer.Sound('img/game_over.wav')
game_over_fx.set_volume(0.05)




def draw_text(text, font,text_col, x, y):
    image = font.render(text, True, text_col)
    screen.blit(image, (x,y))

# function to reset level
def reset_level(level):
    player.reset(100,screen_height - 130)
    blob_group.empty()
    lava_group.empty()
    exit_group.empty()
    platform_group.empty()
    if path.exists(f'level{level}_data'):

        pickle_in = open(f'level{level}_data', 'rb')
        world_data = pickle.load(pickle_in)
    world = World(world_data)
    return world


# input class to generate input boxes

# class InputBox ():
#     def __init__(self, x, y, width, height, text='' ):
#         self.rect = pygame.Rect(x, y, width, height)
#         self.color = blue
#         self.text = text
#         self.txt_surface = FONT.render(text, True, self.color)
#         self.active = False
#     def handle_event(self, event):
#         if event.type == pygame.MOUSEBUTTONDOWN:
#             # If the user clicked on the input rect
#             if self.rect.collidepoint(event.pos):
#                 self.active = not self.active
#             else:
#                 self.active = False
#             # render changes in text
#             self.txt_surface = FONT.render(self.text, True, self.color)
#     def update(self):
#         # Resize the box if the text is too long.
#         width = max(200, self.txt_surface.get_width()+10)
#         self.rect.w = width
#     def draw(self, screen):
#         # Blit the text.
#         screen.blit(self.txt_surface, (self.rect.x+5, self.rect.y+5))
#         # Blit the rect.
#         pygame.draw.rect(screen, self.color, self.rect, 2)



class Button ():
    def __init__(self, x, y , image):
        self.image = image
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.clicked = False
    def draw(self):
        action = False
        # get mouse pos
        pos = pygame.mouse.get_pos()

        #check mouse over and clicked conditions
        if self.rect.collidepoint(pos):
            if pygame.mouse.get_pressed()[0] == 1 and self.clicked == False:
                action = True
                self.clicked = True
        if pygame.mouse.get_pressed()[0] == 0:
            self.clicked = False
        #draw the button
        screen.blit(self.image, self.rect)
        return action


class Player():
    def __init__(self,x, y):
        self.reset(x,y)

    def update(self, game_over):
        
        dx = 0
        dy = 0        
        walk_cooldown = 5
        col_thresh = 20


        # get inputs

        if game_over == 0:
        
            key = pygame.key.get_pressed()
            if key[pygame.K_SPACE] and self.jumped==False and self.in_air == False:
                jump_fx.play()
                self.vel_y = -16
                self.jumped = True

            if key[pygame.K_SPACE] == False:
                self.jumped=False
            if key[pygame.K_LEFT]:
                dx -= 5
                self.counter +=1
                self.direction= -1
                
            if key[pygame.K_RIGHT]:
                self.counter +=1
                self.direction = 1            
                dx += 5
            if key[pygame.K_LEFT] == False and key[pygame.K_RIGHT] == False:
                self.counter = 0
                self.index = 0
                if self.direction == 1:              
                    self.image = self.images_right[self.index]
                
                if self.direction == -1:
                    self.image = self.images_left[self.index]
                    
                    
            
                
            # handle animations
            if self.counter > walk_cooldown:
                self.counter = 0
                self.index+=1
                if self.index >= len(self.images_right):
                    self.index = 0
            if self.direction == 1:              
                self.image = self.images_right[self.index]
                
            if self.direction == -1:
                self.image = self.images_left[self.index]
            # add gravity
            self.vel_y +=1  
            if self.vel_y > 10:
                self.vel_y = 10
            dy += self.vel_y     
            #check for collisions
            self.in_air = True

            for tile in world.tile_list:
                # check in x direction
                if tile[1].colliderect(self.rect.x + dx, self.rect.y, self.width, self.height):
                    dx = 0

                # check in y direction
                if tile[1].colliderect(self.rect.x, self.rect.y + dy, self.width, self.height):
                    #check if below the ground jumped
                    if self.vel_y < 0:
                        dy = tile[1].bottom - self.rect.top
                        self.vel_y = 0

                    # check if above the ground falling
                    elif self.vel_y >= 0:
                        dy = tile[1].top - self.rect.bottom
                        self.vel_y = 0
                        self.in_air = False
                # check for collision with enemies
                if pygame.sprite.spritecollide(self, blob_group, False):
                    game_over = -1
                    game_over_fx.play()
                # check for collisions with lava
                if pygame.sprite.spritecollide(self, lava_group, False):
                    game_over = -1
                    game_over_fx.play()
                # check collision with exit
                if pygame.sprite.spritecollide(self, exit_group, False):
                    game_over = 1

                
                # check for collisions with platforms 
                for platform in platform_group:
                    # collisions in x direction
                    if platform.rect.colliderect(self.rect.x + dx, self.rect.y, self.width, self.height):
                        dx = 0

                    # collisions in y direction
                    if platform.rect.colliderect(self.rect.x, self.rect.y + dy, self.width, self.height):
                        # check if bellow platform
                        if abs((self.rect.top + dy) - platform.rect.bottom) < col_thresh:
                            self.vel_y = 0
                            dy = platform.rect.bottom - self.rect.top
                        # check if above platform
                        elif abs((self.rect.bottom + dy) - platform.rect.top) < col_thresh:
                            self.rect.bottom = platform.rect.top - 1
                            dy = 0
                            self.in_air = False
                        # move sideways with  platform
                        if platform.move_x != 0:
                            self.rect.x += platform.direction                   
            
         
            
            # update player cor
            
            self.rect.x += dx
            self.rect.y += dy
            
    
        
        elif game_over == -1:
             self.image = self.dead_image
             draw_text('game over!', font, blue, (screen_width // 2) - 200, screen_height // 2)
             if self.rect.y > 200:
                 self.rect.y -=5
             
        
        # draw player
        screen.blit(self.image, self.rect)
        return game_over
    def reset(self, x, y):
        self.images_right = []
        self.images_left = []
        
        self.index = 0
        self.counter = 0
        
        for num in range(1,5):
            img_right = pygame.image.load(f'img/guy{num}.png')
            img_right = pygame.transform.scale(img_right, (40,80))
            img_left = pygame.transform.flip(img_right, True, False)
            self.images_right.append(img_right)
            self.images_left.append(img_left)

        self.dead_image = pygame.image.load('img/ghost.png')    
        self.image = self.images_right[self.index]    
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        self.vel_y = 0
        self.jumped = False
        self. direction = 0
        self.in_air = True
class World():
    def __init__(self, data):
        self.tile_list = []

        #load images
        dirt_img = pygame.image.load('img/dirt.png')
        grass_img = pygame.image.load('img/grass.png')

        row_count = 0
        for row in data:
            col_count = 0
            for tile in row:
                if tile == 1:
                    img = pygame.transform.scale(dirt_img, (tile_size, tile_size))
                    img_rect = img.get_rect()
                    img_rect.x = col_count * tile_size
                    img_rect.y = row_count * tile_size
                    tile = (img, img_rect)
                    self.tile_list.append(tile)
                if tile == 2:
                    img = pygame.transform.scale(grass_img, (tile_size, tile_size))
                    img_rect = img.get_rect()
                    img_rect.x = col_count * tile_size
                    img_rect.y = row_count * tile_size
                    tile = (img, img_rect)
                    self.tile_list.append(tile)
                if tile == 3:
                    blob = Enemy(col_count* tile_size, row_count * tile_size + 15)
                    blob_group.add(blob)
                if tile == 4:
                    platform = Platform(col_count * tile_size, row_count * tile_size, 1,0)
                    platform_group.add(platform)
                if tile == 5:
                    platform = Platform(col_count * tile_size, row_count * tile_size, 0, 1)
                    platform_group.add(platform)
                if tile == 6:
                    lava = Lava(col_count* tile_size, row_count * tile_size+ (tile_size //2))
                    lava_group.add(lava)
                if tile == 7:
                    coin = Coin(col_count* tile_size + (tile_size // 2 ), row_count * tile_size+ (tile_size //2))
                    coin_group.add(coin)

                if tile == 8:
                    exit = Exit(col_count * tile_size, row_count * tile_size - (tile_size // 2))
                    exit_group.add(exit)

                col_count += 1
            row_count += 1

    def draw(self):
        for tile in self.tile_list:
            screen.blit(tile[0], tile[1])

class Enemy(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('img/blob.png')
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.direction = 1
        self.move_counter = 0
    def update(self):
        self.rect.x += self.direction
        self.move_counter+= 1
        if abs(self.move_counter) > 50:
            self.direction *= -1
            self.move_counter *= -1

class Platform(pygame.sprite.Sprite):
    def __init__(self, x, y, move_x, move_y):
        pygame.sprite.Sprite.__init__(self)
        image = pygame.image.load('img/platform.png')
        self.image = pygame.transform.scale(image, (tile_size, tile_size // 2))
        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.x = x
        self.move_counter = 0
        self.direction = 1
        self.move_x = move_x
        self.move_y = move_y

    def update(self):
        self.rect.x += self.move_x * self.direction
        self.rect.y += self.move_y * self.direction

        self.move_counter+= 1
        if abs(self.move_counter) > 50:
            self.direction *= -1
            self.move_counter *= -1

        
class Lava(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        image = pygame.image.load('img/lava.png')
        self.image = pygame.transform.scale(image, (tile_size, tile_size // 2))
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

class Exit(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        image = pygame.image.load('img/exit.png')
        self.image = pygame.transform.scale(image, (tile_size, int(tile_size *1.5)))
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
class Coin(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        image = pygame.image.load('img/coin.png')
        self.image = pygame.transform.scale(image, (tile_size  // 2, tile_size // 2))
        self.rect = self.image.get_rect()
        self.rect.center = (x,y)
        
    

player = Player(100,screen_height - 130)

blob_group = pygame.sprite.Group()
platform_group = pygame.sprite.Group()
lava_group = pygame.sprite.Group()
exit_group = pygame.sprite.Group()
coin_group = pygame.sprite.Group()

# create dummy coin for showing the score
score_coin = Coin(tile_size // 2, tile_size // 2)
coin_group.add(score_coin)

# create input boxes

# name_box = InputBox(100, 100, 140, 32)
# password_box = InputBox(100, 300, 140, 32)


# load in level data and create world
if path.exists(f'level{level}_data'):

    pickle_in = open(f'level{level}_data', 'rb')
    world_data = pickle.load(pickle_in)
world = World(world_data)
start_time = time.time()

# create the buttons

restart_button = Button(screen_width //2-50, screen_height // 2+100,restart_image)
exit_button = Button(screen_width //2+150, screen_height // 2,exit_image)
start_button = Button(screen_width //2-350, screen_height // 2,start_image)
highscores_button = Button(screen_width //2-350, screen_height // 2 +150,highscores_image)




# pygame part
run = True
while run:
   



   clock.tick(fps)
   
   screen.blit(sky_image, (0,0))
   screen.blit(sun_image,(100, 100))
   if main_menu == True:
       if highscores_button.draw():
           user.show_highscores()
           user.export_highscores()
        #    user.show_results = True
        
                  
       if exit_button.draw():
         run = False
       if start_button.draw():
         main_menu = False
   else:

    world.draw()
    if game_over == 0:
        blob_group.update()
        platform_group.update()
        # update score of coins
        #check if a coin has been collected
        if pygame.sprite.spritecollide(player, coin_group,True):
            score += 1
            user.coin_score += 1
            coin_fx.play()
        draw_text('X ' + str(user.coin_score), font_score, white, tile_size -10, 10)  
    
    blob_group.draw(screen)
    lava_group.draw(screen)
    exit_group.draw(screen)
    coin_group.draw(screen)
    platform_group.draw(screen)


    game_over = player.update(game_over)
    
    # draw buttons when player has died
    if game_over == -1:
            if restart_button.draw():
                world_data=[]
                world = reset_level(level)
                game_over = 0
                score = 0
                user.coin_score = 0
    # if player has won the level
    if game_over == 1:
        user.time_score = round(time.time()- start_time, 4)
        if level <= max_levels:
            user.addScore(level)
        # reset game and prepare next level
        level+= 1
        user.coin_score = 0
        
        if level <= max_levels:
            #reset levels
            world_data=[]
            world = reset_level(level)
            game_over = 0
            start_time = time.time()
        else:
            draw_text('You Win!', font, blue, (screen_width // 2) - 140, screen_height // 2)

            if restart_button.draw():
                level = 0
                world_data=[]
                world = reset_level(level)
                game_over = 0
                score = 0
                user.coin_score = 0
                main_menu = True


                
            # restart game

        
   for event in pygame.event.get():
       if event.type == pygame.QUIT:
           run = False
       
   pygame.display.update() 

pygame.quit()
