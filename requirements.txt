bcrypt==3.2.0
cffi==1.15.0
mysql-connector-python==8.0.27
protobuf==3.19.1
pycparser==2.20
pygame==2.0.2
python-dotenv==0.19.1
six==1.16.0
